package vue;

import elements.Explosion;
import elements.Contenue;
import elements.Position;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

// Visualiser une explosion dans le champ de bataille
public class VueExplosion extends JLabel
{
      private Image explosion;
      private ImageIcon iconexplosion;
   
      public VueExplosion(Position p)
      {
           super();
           iconexplosion = new ImageIcon(getClass().getResource("../../image/explosion.png"));
           this.explosion = this.iconexplosion.getImage();
      }
      public Image getExplosion()
      {
            return this.explosion;
      }
    
}
