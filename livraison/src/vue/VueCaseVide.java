package vue;

import elements.CaseVide;
import elements.Position;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Image;
import java.io.IOException;

// Visualiser une case vide dans le champ de bataille
public class VueCaseVide extends CaseVide
{
     

    private Image casevide; 
    private ImageIcon iconcasevide;

    public VueCaseVide(Position p) 
    {
        super(p);
        iconcasevide = new ImageIcon(getClass().getResource("image/caseVide.png"));
        this.casevide = this.iconcasevide.getImage();
    }
        
    public Image getCaseVide()
    {
        return this.casevide;
    }
    public void setImage(Image img)
    {
        this.casevide=img;
    }

}
    

