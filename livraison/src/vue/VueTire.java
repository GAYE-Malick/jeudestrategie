package vue;


import elements.Position;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

// Visualiser un tir dans le champ de bataille
public class VueTire extends JLabel
{
   private Image tire;
   private ImageIcon icontire;
   
   public VueTire(Position p)
   {
      super();
      icontire = new ImageIcon(getClass().getResource("image/tire.png"));
      this.tire = this.icontire.getImage();
   }
        
   public Image getTire()
   {
      return this.tire;
   }
}
