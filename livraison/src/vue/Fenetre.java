
package vue;

import champdebataille.Plateau;
import javax.swing.JFrame;

public class Fenetre  extends JFrame 
{
    VuePrincipale vueBomberman;
    Plateau champdebataille;
    
    public Fenetre(VuePrincipale vueBomberman,Plateau champdebataille)
    { 
        
        
        this.setTitle("TERRAIN");
        
        this.champdebataille=champdebataille;
        
        this.vueBomberman=vueBomberman;
        
        this.setContentPane(this.vueBomberman);
        
        this.setSize(1100, 700);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        champdebataille.addObserver(vueBomberman);
        
        this.setVisible(true);
    }

}

    

