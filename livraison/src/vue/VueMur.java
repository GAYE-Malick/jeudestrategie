package vue;

import elements.Mur;
import elements.Position;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
// Visualiser la position d'un mur dans le champ de bataille
    public class VueMur extends Mur
    {
        private Image mur; 
        private ImageIcon iconMur;

        public VueMur(Position p) 
        {
           super(p);
           iconMur = new ImageIcon(getClass().getResource("image/murM.png"));
           this.mur = this.iconMur.getImage();  
        }

        public Image getMur()
        {
            return this.mur;
        }
    }

    
