package vue;

import elements.Position;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

// Visualiser l'explosion d'une mine dans le champ de bataille
public class VueExplosionMine extends JLabel
{
   private Image explosionmine;
   private ImageIcon iconexplosionmine;
   public VueExplosionMine(Position p)
   {
      super();
      iconexplosionmine = new ImageIcon(getClass().getResource("image/explosionmine.png"));
      this.explosionmine = this.iconexplosionmine.getImage();
   }

   public Image getExplosionMine()
   {
      return this.explosionmine;
   }
}
