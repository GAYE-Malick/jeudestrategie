
package champdebataille.armes;

/*Cette classe est un type de mine invisible */
public class MineInvisible extends Mine
{
    
    public MineInvisible()
    {
        super();
       this.type = "MineInvisible";
    }

}
