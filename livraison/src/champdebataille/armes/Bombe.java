
package champdebataille.armes;

import elements.Position;

public abstract class Bombe extends Explosable
{
    
    public static final int PUISSANCE_MAX = 50; // Puissance maximale de la bombe
    private static final int PORTEE_MAX = 9; // Portée maximale de la bombe
    public static final int DELAI_MAX = 3; // Délai d'explosion
      
    protected int compteARebours;
       
    public Bombe()
    {
        super();
        puissance = PUISSANCE_MAX;
        portee = PORTEE_MAX;
    }

    @Override
    public void activerExplosif(Position p)
    {
        this.setPosition(p);
        this.compteARebours = DELAI_MAX;
    }
   
    /**
     * Permet de decrémenter le compteur de la bombe
     */
    public void decremente()
    {
        this.compteARebours -= 1;
    }
    /**
     * 
     * @return Le compteur courant de la bombe
     */
    public int getCompteARebours()
    {
        return this.compteARebours;
    }
}