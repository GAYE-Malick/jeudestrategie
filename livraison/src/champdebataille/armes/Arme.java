
package champdebataille.armes;

public abstract class Arme 
{

    protected String type; // type de l'arme
    protected int puissance; // puissance de l'arme
    protected int portee; //portée de l'arme


    /**
     * retourne le type de l'arme
     * @return Le type de l'arme  
     */
    public String getType() 
    {
        return this.type;
    }

    /**
     * Retourne la puissance de l'arme
     * @return La puissance de l'arme 
     */
    public int getPuissance() 
    {
        return this.puissance;
    }

    /**
     * Retourne la portée de l'arme
     * @return la portée de l'arme
     */
    public int getPortee() 
    {
        return this.portee;
    }
    /**
     * Modifie la puissance de l'arme par la puissance passée en paramètre 
     * @param puissance 
     */
    public void setPuissance(int puissance) 
    {
        this.puissance = puissance;
    }

    /**
     * Modifie la portée de l'arme par la portée passée en paramètre
     * @param portee 
     */
    public void setPortee(int portee) 
    {
        this.portee = portee;
    }
}
