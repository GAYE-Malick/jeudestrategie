
package champdebataille.jfactory;

import champdebataille.Combattant;
import champdebataille.Sniper;
import champdebataille.jstrategy.StrategieSniper;
import elements.Position;




//combattant de type Sniper
public class SniperFactory extends CombattantFactory
{

    @Override
    public Combattant creeCombattant(String pseudo, Position p) 
    {
        return new Sniper(new StrategieSniper(),pseudo,p);
    }
    
}
