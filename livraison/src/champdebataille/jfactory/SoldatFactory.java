
package champdebataille.jfactory;

import champdebataille.Combattant;
import champdebataille.Soldat;
import champdebataille.jstrategy.StrategieSoldat;
import elements.Position;





//combattant de type Soldat

public class SoldatFactory extends CombattantFactory
{

    @Override
    public Combattant creeCombattant(String pseudo, Position p) 
    {
        return new Soldat(new StrategieSoldat(),pseudo,p);
    }
    
}
