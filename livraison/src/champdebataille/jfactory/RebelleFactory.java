
package champdebataille.jfactory;

import champdebataille.Combattant;
import champdebataille.Rebelle;
import champdebataille.jstrategy.StrategieRebelle;
import elements.Position;



// combattant de type rebelle
public class RebelleFactory extends CombattantFactory
{

    @Override
    public Combattant creeCombattant(String pseudo, Position p) 
    {
        return new Rebelle(new StrategieRebelle(),pseudo,p);
    }
    
}
