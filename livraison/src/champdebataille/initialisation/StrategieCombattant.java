package champdebataille.initialisation;


import champdebataille.ChampDeBataille;
import champdebataille.Combattant;


//Strategie des actions du combattant
public interface StrategieCombattant 
{
    void action(ChampDeBataille champ,Combattant c);
    String getDecision();
    
}
