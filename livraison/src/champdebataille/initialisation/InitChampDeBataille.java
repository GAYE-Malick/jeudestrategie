
package champdebataille.initialisation;

import champdebataille.ChampDeBataille;

public interface InitChampDeBataille 
{
    
    void init(ChampDeBataille champ);
    
}
